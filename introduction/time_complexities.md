# Time complexity cheatsheet

| Algorithm | Time complexity |
| -- | -- |
| Tower of hanoi | 2^n |
| String permutations | n*n! |
